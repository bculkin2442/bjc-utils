package bjc.utils.data.bst;

import java.util.Comparator;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Predicate;

public interface ITreePart<T> {
	public enum TreeLinearizationMethod {
		INORDER, POSTORDER, PREORDER
	}
	
	void add(T dat, Comparator<T> comp);
	<E> E collapse(Function<T, E> f, BiFunction<E, E, E> bf);
	boolean contains(T data, Comparator<T> cmp);
	T data();
	void delete(T dat, Comparator<T> cmp);
	boolean directedEach(DirectedSearchFunction<T> ds);
	boolean forEach(TreeLinearizationMethod tlm, Predicate<T> c);
}
