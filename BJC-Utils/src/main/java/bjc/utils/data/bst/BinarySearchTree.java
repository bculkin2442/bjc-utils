package bjc.utils.data.bst;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.function.Predicate;

import bjc.utils.data.bst.ITreePart.TreeLinearizationMethod;

public class BinarySearchTree<T> {
	private Comparator<T>	comp;
	private int				nCount;
	private ITreePart<T>	root;

	public BinarySearchTree(Comparator<T> cmp) {
		nCount = 0;
		comp = cmp;
	}

	public void addNode(T dat) {
		nCount++;
		if (root == null) {
			root = new TreeNode<T>(dat, null, null);
		} else {
			root.add(dat, comp);
		}
	}

	public void balance() {
		ArrayList<T> elms = new ArrayList<>(nCount);
		
		root.forEach(TreeLinearizationMethod.INORDER, e -> elms.add(e));
		
		root = null;
		
		int piv = elms.size() / 2;
		int adj = 0;
		
		while((piv - adj) >= 0 && (piv + adj) < elms.size()) {
			if(root == null) {
				root = new TreeNode<T>(elms.get(piv), null, null);
			} else {
				root.add(elms.get(piv + adj), comp);
				root.add(elms.get(piv - adj), comp);
			}
			
			adj++;
		}
		
		if((piv - adj) >= 0) {
			root.add(elms.get(piv - adj), comp);
		} else if((piv + adj) < elms.size()) {
			root.add(elms.get(piv + adj), comp);
		}
	}

	public ITreePart<T> getRoot() {
		return root;
	}
	
	public void deleteNode(T dat) {
		nCount--;
		root.delete(dat, comp);
	}

	public boolean findNode(T dat) {
		return root.contains(dat, comp);
	}

	public void traverse(TreeLinearizationMethod tlm, Predicate<T> p) {
		root.forEach(tlm, p);
	}

	public void trim() {
		List<T> nds = new ArrayList<>(nCount);

		traverse(TreeLinearizationMethod.PREORDER, d -> {
			nds.add(d);
			return true;
		});

		root = null;

		nds.forEach(d -> addNode(d));
	}
}
