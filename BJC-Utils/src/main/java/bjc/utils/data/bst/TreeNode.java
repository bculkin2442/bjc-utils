package bjc.utils.data.bst;

import static bjc.utils.data.bst.DirectedSearchFunction.DirectedSearchResult.*;

import java.util.Comparator;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Predicate;

public class TreeNode<T> extends TreeLeaf<T> {
	private ITreePart<T>	left;
	private ITreePart<T>	right;

	public TreeNode(T data, ITreePart<T> left, ITreePart<T> right) {
		super(data);
		this.left = left;
		this.right = right;
	}

	@Override
	public void add(T dat, Comparator<T> comp) {
		switch (comp.compare(data, dat)) {
			case -1:
				if (left == null) {
					left = new TreeNode<T>(dat, null, null);
				} else {
					left.add(dat, comp);
				}
			case 0:
				if (deleted) {
					deleted = false;
				} else {
					throw new IllegalArgumentException(
							"Can't add duplicate values");
				}
			case 1:
				if (right == null) {
					right = new TreeNode<T>(dat, null, null);
				} else {
					right.add(dat, comp);
				}
		}
	}

	@Override
	public <E> E collapse(Function<T, E> f, BiFunction<E, E, E> bf) {
		E tm = f.apply(data);

		if (left != null) {
			if (right != null) {
				return bf.apply(tm, bf.apply(left.collapse(f, bf),
						right.collapse(f, bf)));
			} else {
				return bf.apply(tm, left.collapse(f, bf));
			}
		} else {
			if(right != null) {
				return bf.apply(tm, right.collapse(f, bf));
			} else {
				return tm;
			}
		}
	}

	@Override
	public boolean contains(T data, Comparator<T> cmp) {
		return directedEach(ds -> {
			switch (cmp.compare(data, ds)) {
				case -1:
					return LEFT;
				case 0:
					return deleted ? NOTFOUND : FOUND;
				case 1:
					return RIGHT;
				default:
					return NOTFOUND;
			}
		});
	}

	@Override
	public void delete(T dat, Comparator<T> cmp) {
		directedEach(new DirectedSearchFunction<T>() {
			@Override
			public DirectedSearchResult search(T ds) {
				switch (cmp.compare(data, dat)) {
					case -1:
						return left == null ? NOTFOUND : LEFT;
					case 0:
						deleted = true;
						return NOTFOUND;
					case 1:
						return right == null ? NOTFOUND : RIGHT;
					default:
						return DirectedSearchResult.NOTFOUND;
				}
			}
		});
	}

	@Override
	public boolean directedEach(DirectedSearchFunction<T> ds) {
		switch (ds.search(data)) {
			case FOUND:
				return true;
			case LEFT:
				return left.directedEach(ds);
			case RIGHT:
				return right.directedEach(ds);
			case NOTFOUND:
				return false;
			default:
				return false;
		}
	}

	@Override
	public boolean forEach(TreeLinearizationMethod tlm, Predicate<T> c) {
		switch (tlm) {
			case PREORDER:
				return preorderTraverse(tlm, c);
			case INORDER:
				return inorderTraverse(tlm, c);
			case POSTORDER:
				return postorderTraverse(tlm, c);
			default:
				throw new IllegalArgumentException(
						"Passed an incorrect TreeLinearizationMethod.");
		}
	}

	public ITreePart<T> getLeft() {
		return left;
	}

	public ITreePart<T> getRight() {
		return right;
	}

	private boolean inorderTraverse(TreeLinearizationMethod tlm,
			Predicate<T> c) {

		return ((left == null ? true : left.forEach(tlm, c))
				&& (deleted ? true : c.test(data))
				&& (right == null ? true : right.forEach(tlm, c)));
	}

	private boolean postorderTraverse(TreeLinearizationMethod tlm,
			Predicate<T> c) {

		return ((left == null ? true : left.forEach(tlm, c))
				&& (right == null ? true : right.forEach(tlm, c))
				&& (deleted ? true : c.test(data)));

	}

	private boolean preorderTraverse(TreeLinearizationMethod tlm,
			Predicate<T> c) {

		return ((deleted ? true : c.test(data))
				&& (left == null ? true : left.forEach(tlm, c))
				&& (right == null ? true : right.forEach(tlm, c)));
	}

	public void setLeft(ITreePart<T> left) {
		this.left = left;
	}

	public void setRight(ITreePart<T> right) {
		this.right = right;
	}
}
