package bjc.utils.data.bst;

import java.util.Comparator;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Predicate;

public class TreeLeaf<T> implements ITreePart<T> {
	protected T data;
	protected boolean deleted;
	
	public TreeLeaf(T dat) {
		data = dat;
	}
	
	@Override
	public void add(T dat, Comparator<T> comp) {
		throw new IllegalArgumentException("Can't add to a leaf.");
	}

	@Override
	public <E> E collapse(Function<T, E> f, BiFunction<E, E, E> bf) {
		return f.apply(data);
	}

	@Override
	public boolean contains(T data, Comparator<T> cmp) {
		return this.data.equals(data);
	}
	
	public T data() {
		return data;
	}
	
	@Override
	public void delete(T dat, Comparator<T> cmp) {
		if(data.equals(dat)) {
			deleted = true;
		}
	}
	
	@Override
	public boolean directedEach(DirectedSearchFunction<T> ds) {
		switch(ds.search(data)) {
			case FOUND:
				return true;
			default:
				return false;
		}
	}
	
	public boolean forEach(TreeLinearizationMethod tlm, Predicate<T> c) {
		return c.test(data);
	}
}
