package bjc.utils.data.bst;

@FunctionalInterface
public interface DirectedSearchFunction<T> {
	public enum DirectedSearchResult {
		FOUND, LEFT, NOTFOUND, RIGHT
	}
	
	public DirectedSearchResult search(T data);
}
