package bjc.utils.data;

public class GenHolder<E> {
	public E held;
	
	public GenHolder() {
		held = null;
	}
	
	public GenHolder(E hld) {
		held = hld;
	}
}
