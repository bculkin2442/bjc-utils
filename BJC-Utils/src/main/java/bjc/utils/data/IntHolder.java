package bjc.utils.data;

public class IntHolder {
	public int held;
	
	public IntHolder(int h) {
		held = h;
	}
	
	public int inc() {
		return held++;
	}
	
	public void sub(int v) {
		held -= v;
	}
}
