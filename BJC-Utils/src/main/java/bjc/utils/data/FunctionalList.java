package bjc.utils.data;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;

import bjc.utils.FunctionalStringTokenizer;

public class FunctionalList<E> {
	public static <E> FunctionalList<E> fromString(
			FunctionalStringTokenizer fst, Function<String, E> f) {
		FunctionalList<E> r = new FunctionalList<>();

		fst.forEachToken(tk -> r.add(f.apply(tk)));

		return r;
	}

	private List<E> wrap;

	@SafeVarargs
	public FunctionalList(E... items) {
		wrap = new ArrayList<>(items.length);

		for (E item : items) {
			wrap.add(item);
		}
	}

	public FunctionalList(FunctionalList<E> fl) {
		wrap = fl.wrap;
	}

	private FunctionalList(int sz) {
		wrap = new ArrayList<>(sz);
	}

	public FunctionalList(List<E> l) {
		wrap = l;
	}

	public boolean add(E item) {
		return wrap.add(item);
	}

	public boolean allMatch(Predicate<E> p) {
		for (E item : wrap) {
			if (!p.test(item)) {
				return false;
			}
		}
		return true;
	}

	public boolean anyMatches(Predicate<E> p) {
		for (E item : wrap) {
			if (p.test(item)) {
				return true;
			}
		}
		return false;
	}

	public E first() {
		return wrap.get(0);
	}

	public <T> FunctionalList<T> flatMap(Function<E, List<T>> f) {
		FunctionalList<T> fl = new FunctionalList<>(this.wrap.size());

		forEach(e -> {
			f.apply(e).forEach(e2 -> {
				fl.add(e2);
			});
		});

		return fl;
	}

	public void forEach(Consumer<E> c) {
		wrap.forEach(c);
	}

	public void forEachIndexed(BiConsumer<Integer, E> c) {
		int i = 0;

		for (E e : wrap) {
			c.accept(i++, e);
		}
	}

	public E getByIndex(int i) {
		return wrap.get(i);
	}

	public List<E> getInternal() {
		return wrap;
	}

	public boolean isEmpty() {
		return wrap.isEmpty();
	}

	public <T> FunctionalList<T> map(Function<E, T> f) {
		FunctionalList<T> fl = new FunctionalList<>(this.wrap.size());

		forEach(e -> fl.add(f.apply(e)));

		return fl;
	}

	public E randItem(Random rnd) {
		return wrap.get(rnd.nextInt(wrap.size()));
	}

	public E search(E key, Comparator<E> cmp) {
		int res = Collections.binarySearch(wrap, key, cmp);

		return wrap.get(res);
	}

	public void sort(Comparator<E> cmp) {
		Collections.sort(wrap, cmp);
	}

	public String toString() {
		StringBuilder sb = new StringBuilder("(");

		forEach(s -> sb.append(s + ", "));

		sb.deleteCharAt(sb.length());
		sb.append(")");

		return sb.toString();
	}
	
	public <T, F> F reduceAux(T val, BiFunction<E, T, T> bf, Function<T, F> finl) {
		GenHolder<T> acum = new GenHolder<>(val);
		
		wrap.forEach(e -> {
			acum.held = bf.apply(e, acum.held);
		});
		
		return finl.apply(acum.held);
	}
	
	public <T, F> FunctionalList<F> combineWith(FunctionalList<T> l, BiFunction<E, T, F> bf) {
		FunctionalList<F> r = new FunctionalList<>();
		
		Iterator<T> i2 = l.wrap.iterator();
		
		for (Iterator<E> i1 = wrap.iterator(); i1.hasNext() && i2.hasNext();) {
			r.add(bf.apply(i1.next(), i2.next()));
		}
		
		return r;
	}
}
