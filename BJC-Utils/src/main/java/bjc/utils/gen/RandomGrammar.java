package bjc.utils.gen;

import java.util.HashMap;

import bjc.utils.data.FunctionalList;

public class RandomGrammar<E> extends WeightedGrammar<E> {

	public final void makeRule(E rule,
			FunctionalList<FunctionalList<E>> cases) {
		super.addRule(rule);
		
		cases.forEach(cse -> super.addCase(rule, 1, cse));
	}

	@SafeVarargs
	public final void makeRule(E rule, FunctionalList<E>... cases) {
		super.addRule(rule);
		
		for (FunctionalList<E> cse : cases) {
			super.addCase(rule, 1, cse);
		}
	}

	@SafeVarargs
	public final void addCases(E rule, FunctionalList<E>... cases) {		
		for (FunctionalList<E> cse : cases) {
			super.addCase(rule, 1, cse);
		}
	}

	public RandomGrammar() {
		rules = new HashMap<>();
	}
}
