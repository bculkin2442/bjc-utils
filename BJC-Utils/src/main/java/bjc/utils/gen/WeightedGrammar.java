package bjc.utils.gen;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.function.Function;

import bjc.utils.data.FunctionalList;

public class WeightedGrammar<E> {
	protected Map<E, WeightedRandom<FunctionalList<E>>>	rules;
	protected Map<E, WeightedGrammar<E>>				subgrammars;

	public boolean addSubGrammar(E name, WeightedGrammar<E> subG) {
		if (subgrammars.containsKey(name)) {
			return false;
		} else {
			subgrammars.put(name, subG);
			return true;
		}
	}

	public boolean addRule(E name, WeightedRandom<FunctionalList<E>> rnd) {
		if (rules.containsKey(name)) {
			return false;
		} else {
			rules.put(name, rnd);
			return true;
		}
	}

	public boolean addRule(E name) {
		return addRule(name, new WeightedRandom<>(new Random()));
	}

	public void addCase(E rule, int prob, FunctionalList<E> cse) {
		WeightedRandom<FunctionalList<E>> rn = rules.get(rule);

		rn.addProb(prob, cse);
	}

	public FunctionalList<FunctionalList<E>> debugVals(E rl) {
		FunctionalList<FunctionalList<E>> fl = new FunctionalList<>();

		WeightedRandom<FunctionalList<E>> random = rules.get(rl);

		for (int i = 0; i < 10; i++) {
			fl.add(random.genVal());
		}

		return fl;
	}

	public <T> FunctionalList<T> genGeneric(E initRule, Function<E, T> f,
			T spacer) {
		FunctionalList<T> r = new FunctionalList<>();

		if (subgrammars.containsKey(initRule)) {
			subgrammars.get(initRule).genGeneric(initRule, f, spacer)
					.forEach(rp -> {
						r.add(rp);
						r.add(spacer);
					});
		} else if (rules.containsKey(initRule)) {
			rules.get(initRule).genVal().forEach(
					rp -> genGeneric(rp, f, spacer).forEach(rp2 -> {
						r.add(rp2);
						r.add(spacer);
					}));
		} else {
			r.add(f.apply(initRule));
			r.add(spacer);
		}

		return r;
	}

	public FunctionalList<E> genList(E initRule, E spacer) {
		return genGeneric(initRule, s -> s, spacer);
	}

	public WeightedGrammar() {
		rules = new HashMap<>();
	}
}
