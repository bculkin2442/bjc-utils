package bjc.utils.gen;

import java.util.Random;

import bjc.utils.data.FunctionalList;
import bjc.utils.data.GenHolder;
import bjc.utils.data.IntHolder;

public class WeightedRandom<E> {
	private Random src;

	private FunctionalList<Integer>	probs;
	private FunctionalList<E>		results;

	private int totalChance;

	public WeightedRandom(Random sr) {
		probs = new FunctionalList<>();
		results = new FunctionalList<>();

		src = sr;
	}

	public void addProb(int chance, E res) {
		probs.add(chance);
		results.add(res);

		totalChance += chance;
	}

	public E genVal() {
		IntHolder v = new IntHolder(src.nextInt(totalChance));
		GenHolder<E> res = new GenHolder<E>();
		GenHolder<Boolean> bl = new GenHolder<>(true);

		probs.forEachIndexed((i, p) -> {
			if (bl.held) {
				if (v.held < p) {
					res.held = results.getByIndex(i);
					bl.held = false;
				} else {
					v.sub(p);
				}
			}
		});

		return res.held;
	}
}
