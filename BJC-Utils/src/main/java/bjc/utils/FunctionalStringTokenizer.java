package bjc.utils;

import java.util.StringTokenizer;
import java.util.function.Consumer;

public class FunctionalStringTokenizer {
	private StringTokenizer inp;

	public FunctionalStringTokenizer(StringTokenizer inp) {
		this.inp = inp;
	}
	
	public void forEachToken(Consumer<String> f) {
		while(inp.hasMoreTokens()) {
			f.accept(inp.nextToken());
		}
	}
	
	public String nextToken() {
		return inp.nextToken();
	}
}
