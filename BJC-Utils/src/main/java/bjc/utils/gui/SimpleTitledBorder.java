package bjc.utils.gui;

import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;

public class SimpleTitledBorder extends TitledBorder {
	private static final long serialVersionUID = -5655969079949148487L;

	public SimpleTitledBorder(String title) {
		super(new EtchedBorder(), title);
	}

}
