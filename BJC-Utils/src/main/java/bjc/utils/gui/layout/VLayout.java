package bjc.utils.gui.layout;

import java.awt.GridLayout;

public class VLayout extends GridLayout {
	private static final long serialVersionUID = -6417962941602322663L;

	public VLayout(int rows) {
		super(rows, 1);
	}
}
