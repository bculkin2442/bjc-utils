package bjc.utils.gui.layout;

import java.awt.GridLayout;

public class HLayout extends GridLayout {
	private static final long serialVersionUID = 1244964456966270026L;

	public HLayout(int cols) {
		super(1, cols);
	}
}
