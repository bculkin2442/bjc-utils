package bjc.utils.gui;

import java.awt.Component;
import java.util.function.Function;
import java.util.function.Predicate;

import javax.swing.JOptionPane;

public class SimpleDialogs {
	public static int getBoundedInt(Component parent, String title,
			String prompt, int lower, int upper) {
		return getValue(parent, title, prompt, s -> {
			try {
				int n = Integer.parseInt(s);
				return (n < upper) && (n > lower);
			} catch (NumberFormatException nfe) {
				return false;
			}
		} , Integer::parseInt);
	}

	public static int getInt(Component parent, String title,
			String prompt) {
		return getValue(parent, title, prompt, s -> {
			try {
				Integer.parseInt(s);
				return true;
			} catch (NumberFormatException nfe) {
				return false;
			}
		} , Integer::parseInt);
	}

	public static String getString(Component parent, String title, String prompt) {
		return JOptionPane.showInputDialog(parent, prompt, title,
				JOptionPane.QUESTION_MESSAGE);
	}

	public static <E> E getValue(Component parent, String title,
			String prompt, Predicate<String> p, Function<String, E> f) {
		String inp = getString(parent, title, prompt);

		while (!p.test(inp)) {
			showError(parent, "I/O Error", "Please enter a valid value");

			inp = getString(parent, title, prompt);
		}

		return f.apply(inp);
	}

	public static int getWhole(Component parent, String title,
			String prompt) {
		return getBoundedInt(parent, title, prompt, 0, Integer.MAX_VALUE);
	}

	public static boolean getYesNo(Component parent, String title,
			String prompt) {
		int res = JOptionPane.showConfirmDialog(parent, prompt, title,
				JOptionPane.YES_NO_OPTION);

		return (res == JOptionPane.YES_OPTION ? true : false);
	}
	
	public static void showError(Component parent, String title,
			String err) {
		JOptionPane.showMessageDialog(parent, err, title,
				JOptionPane.ERROR_MESSAGE);
	}
}
