package bjc.utils.gui;

import java.io.File;
import javax.swing.filechooser.FileFilter;
import java.util.ArrayList;
import java.util.List;

import bjc.utils.data.FunctionalList;

public class ExtensionFileFilter extends FileFilter {
	private FunctionalList<String> extensions;

	public ExtensionFileFilter(String... exts) {
		extensions = new FunctionalList<>(new ArrayList<>(exts.length));

		for (String ext : exts) {
			extensions.add(ext);
		}
	}

	public ExtensionFileFilter(List<String> exts) {
		extensions = new FunctionalList<>(exts);
	}

	@Override
	public boolean accept(File pathname) {
		return extensions.allMatch(pathname.getName()::endsWith);
	}

	@Override
	public String getDescription() {
		return extensions.toString();
	}

}
