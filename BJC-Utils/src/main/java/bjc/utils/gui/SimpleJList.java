package bjc.utils.gui;

import java.util.List;

import javax.swing.DefaultListModel;
import javax.swing.JList;
import javax.swing.ListModel;

public class SimpleJList {
	public static <E> JList<E> buildFromList(List<E> ls) {
		return new JList<E>(buildModel(ls));
	}

	public static <E> ListModel<E> buildModel(List<E> ls) {
		DefaultListModel<E> dlm = new DefaultListModel<>();

		ls.forEach(dlm::addElement);
		
		return dlm;
	}
}
